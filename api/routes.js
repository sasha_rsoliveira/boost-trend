const express = require('express');

const SessionsController = require('./controllers/SessionsController');
const ProductsController = require('./controllers/ProductsController');
const ResultsController = require('./controllers/ResultsController');

const api = express.Router();

api.post('/login', SessionsController.login);
api.post('/register', SessionsController.register);
api.post('/validateToken', SessionsController.validateToken);

api.get('/products', ProductsController.index);
api.post('/products', ProductsController.store);

api.get('/results', ResultsController.index);

module.exports = api;
