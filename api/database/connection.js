const knex = require('knex');
const environment = process.env.ENVIRONMENT || 'development'
const configuraton = require('../../knexfile')[environment];

const connection = knex(configuraton);

module.exports = connection;
