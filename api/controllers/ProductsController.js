const connection = require('../database/connection');

module.exports = {
  async index(request, response) {
    let data = await connection('products');

    return response.json(data);
  },
  async store(request, response) {
    const { name, description, status, category, stock } = request.body;

    try {
      await connection('products').insert({ name, description, status, category, stock })

      return response.json();
    } catch (err) {
      return response.status(400).json({ error: 'Falha ao incluir produto.' });

    }
  }
}
