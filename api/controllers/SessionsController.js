const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const connection = require('../database/connection');
const SECRET = process.env.SECRET;

module.exports = {
  async login(request, response) {
    const { email, password } = request.body;

    try {
      const user = await connection('users').where('email', '=', email).first();

      if (user && bcrypt.compareSync(password, user.password)) {
        const token = jwt.sign({ "id": user.id, "email": user.email, "name": user.name }, SECRET, {
          expiresIn: "1 day"
        })
        const { name } = user

        return response.json({ name, token });
      } else {
        return response.status(400).json({ error: 'E-mail ou senha inválidos' });
      }
    }
    catch (error) {
      return response.status(400).json({ error: 'E-mail ou senha inválidos' });
    }
  },
  async register(request, response) {
    const { name, cnpj, email, password } = request.body;

    const user = await connection('users').where('email', '=', email).first();

    if (user) {
      return response.status(400).json({ error: 'E-mail já cadastrado' });
    }

    try {
      const salt = bcrypt.genSaltSync();
      const passwordHash = bcrypt.hashSync(password, salt);


      await connection('users').insert({
        name,
        cnpj,
        email,
        "password": passwordHash
      })

      return response.json();
    }
    catch (error) {
      return response.status(400).json();
    }
  },
  async validateToken(request, response) {
    const token = request.body.token || ''

    try {
      jwt.verify(token, SECRET, function (error, decoded) {
        return response.status(200).send({ valid: !error })
      })
    }
    catch (error) {
      return response.status(400).json();
    }
  }
}
