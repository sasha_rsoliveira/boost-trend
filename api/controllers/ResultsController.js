const googleTrends = require('google-trends-api');

module.exports = {
  async index(request, response) {
    const { search } = request.query;

    googleTrends.interestByRegion({ keyword: search, startTime: new Date('2017-08-01'), endTime: new Date(), geo: "BR", resolution: 'STATE' })
      .then((res) => {
        return response.json(JSON.parse(res));
      })
      .catch((err) => {
        console.log(err);
      })


  }
}
