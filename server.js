require('dotenv').config({ path: __dirname + '/.env' })
const express = require("express");
const path = require("path");
const cors = require('cors');
const app = express();

const routes = require('./api/routes');

const configs = {
  build: "build",
  forceHTTPS: false,
  port: process.env.PORT || 3000
}

if (configs.forceHTTPS) {
  app.use((req, res, next) => {
    if (req.headers["x-forwarded-proto"] === "http")
      res.redirect(`https://${req.headers.host}${req.url}`);
    else
      next();
  });
}

app.use(cors({
  origin: '*',
  methods: 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
  allowedHeaders: 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
}));

app.use(express.json());
app.use('/api', routes);

app.use(express.static(configs.build));

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, configs.build, "index.html"));
});

app.listen(configs.port, () => {
  console.log(`Listen on port ${configs.port}!`);
});
