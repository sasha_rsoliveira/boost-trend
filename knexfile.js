module.exports = {
  development: {
    client: 'mysql',
    connection: {
      host: '127.0.0.1',
      database: 'boost_trend',
      user: 'boost_trend',
      password: 'soGk8DpVrMXPPtDq'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: './api/database/migrations',
      tableName: 'migrations'
    }
  },

  production: {
    client: 'mysql',
    connection: {
      host: 'mysql669.umbler.com',
      database: 'boost_trend',
      user: 'boost_trend',
      password: 'soGk8DpVrMXPPtDq'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: './api/database/migrations',
      tableName: 'migrations'
    }
  }
};
