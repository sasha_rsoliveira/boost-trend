import React, { useState } from 'react';

import If from '../../components/If';
import api from '../../services/api';

type User = {
  name: string;
  cnpj: string;
  email: string;
  password: string;
  confirm: string;
  remember: boolean;
  agree: boolean;
};

const defaultUser: User = {
  name: "",
  cnpj: "",
  email: "",
  password: "",
  confirm: "",
  remember: false,
  agree: false
};

function LoginOrRegister() {
  const [loginState, setLoginState] = useState(0);
  const [user, setUser] = useState(defaultUser);

  const onUserChange = <P extends keyof User>(prop: P, value: User[P]) => {
    setUser({ ...user, [prop]: value });
  };

  function handleLogin(e: any) {
    e.preventDefault();

    api.post('login', user)
      .then(resp => {
        const { token, name } = resp.data;
        localStorage.setItem('Ae!_token',token);
        localStorage.setItem('Ae!_name',name);
        window.location.reload();
      })
      .catch(err => {
      })
  }

  async function handleRegister(e: any) {
    e.preventDefault();

    api.post('register', user)
      .then(resp => {
      })
      .catch(err => {
      })
  }

  return (
    <div className="login-box">
      <div className="login-logo">
        <a href="/"><b>Impulsiona </b>Aê</a>
      </div>
      <div className="card">
        <div className="card-body login-card-body">
          <p className="login-box-msg">{loginState === 0 ? "Entre para iniciar sua sessão" : "Crie uma conta"}</p>
          <form onSubmit={(e) => loginState ? handleRegister(e) : handleLogin(e)}>
            <If test={loginState === 1}>
              <div className="input-group mb-3">
                <input value={user.name} onChange={e => { onUserChange('name', e.target.value); }} type="text" className="form-control" placeholder="Nome completo" />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-user"></span>
                  </div>
                </div>
              </div>
              <div className="input-group mb-3">
                <input value={user.cnpj} onChange={e => { onUserChange('cnpj', e.target.value); }} type="text" className="form-control" placeholder="CNPJ" />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-user"></span>
                  </div>
                </div>
              </div>
            </If>
            <div className="input-group mb-3">
              <input value={user.email} onChange={e => { onUserChange('email', e.target.value); }} type="email" className="form-control" placeholder="Email" />
              <div className="input-group-append">
                <div className="input-group-text">
                  <span className="fas fa-envelope"></span>
                </div>
              </div>
            </div>
            <div className="input-group mb-3">
              <input value={user.password} onChange={e => { onUserChange('password', e.target.value); }} type="password" className="form-control" placeholder="Senha" />
              <div className="input-group-append">
                <div className="input-group-text">
                  <span className="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <If test={loginState === 1}>
              <div className="input-group mb-3">
                <input value={user.confirm} onChange={e => { onUserChange('confirm', e.target.value); }} type="password" className="form-control" placeholder="Reescreva a senha" />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock"></span>
                  </div>
                </div>
              </div>
            </If>
            <div className="row">
              <div className="col-8">
                {loginState === 0 ?
                  <div className="icheck-primary">
                    <input value={user.remember ? "1" : "0"} onChange={() => { onUserChange('remember', !user.remember); }} type="checkbox" id="remember" />
                    <label htmlFor="remember">Lembre-se</label>
                  </div>
                  :
                  <div className="icheck-primary">
                    <input value={user.agree ? "1" : "0"} onChange={() => { onUserChange('agree', !user.agree); }} type="checkbox" id="agree" />
                    <label htmlFor="agree">Eu concordo com os termos</label>
                  </div>
                }
              </div>
              <div className="col-4">
                <button type="submit" className="btn btn-primary btn-block"> {loginState === 0 ? "Entrar" : "Registrar"}</button>
              </div>
            </div>
          </form>

          {/* <p className="mb-1">
            <a href="forgot-password.html">Esqueci minha senha</a>
          </p> */}
          <p className="mb-0">
            {loginState === 0 ?
              <a href="#" className="text-center" onClick={() => setLoginState(1)}>Crie uma conta</a>
              :
              <a href="#" className="text-center" onClick={() => setLoginState(0)}>Já possui uma conta ?</a>
            }
          </p>
        </div>
      </div>
    </div>

  )
}

export default LoginOrRegister;
