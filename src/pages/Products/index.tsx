import React, { useState } from 'react';

import List from './list';
import Form from './form';

function Panel() {
  const [panelState, setPanelState] = useState('List');
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1 className="m-0 text-dark">Produtos</h1>
              <br />
              {panelState === "List" ?
                <button className="btn btn-primary" onClick={() => setPanelState('Form')}>Novo</button>
                :
                <button className="btn btn-secondary" onClick={() => setPanelState('List')}>Voltar</button>
              }
            </div>
          </div>
        </div>
      </section>
      {panelState === "List" ?
        <List />
        :
        <Form />
      }
    </div>
  )
}

export default Panel;
