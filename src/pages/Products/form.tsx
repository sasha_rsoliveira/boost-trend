import React, { useState } from 'react';

import api from '../../services/api';

type Product = {
  name: string;
  description: string;
  status: string;
  category: string;
  stock: number;
};

const defaultProduct: Product = {
  name: "",
  description: "",
  status: "0",
  category: "",
  stock: 0
};

function ProductForm() {
  const [product, setProduct] = useState(defaultProduct);

  const onProductChange = <P extends keyof Product>(prop: P, value: Product[P]) => {
    setProduct({ ...product, [prop]: value });
  };

  function handleSubmit(e: any) {
    e.preventDefault();

    api.post('products', product)
      .then(resp => {
        alert('Produto criado.');
        setProduct(defaultProduct);
      })
      .catch(err => {
        alert('Falha ao criar produto.');
      })
  }

  return (
    <section className="content">
      <div className="row">
        <div className="col-md-6">
          <div className="card card-primary">
            <div className="card-body">
              <div className="form-group">
                <label htmlFor="inputName">Nome do Produto</label>
                <input value={product.name} onChange={e => { onProductChange('name', e.target.value); }} type="text" id="inputName" className="form-control" />
              </div>
              <div className="form-group">
                <label htmlFor="inputDescription">Descrição do Produto</label>
                <textarea value={product.description} onChange={e => { onProductChange('description', e.target.value); }} id="inputDescription" className="form-control" rows={4}></textarea>
              </div>
              <div className="form-group">
                <label htmlFor="inputStatus">Status</label>
                <select value={product.status} onChange={e => { onProductChange('status', e.target.value); }} className="form-control custom-select">
                  <option selected disabled>Selecione um</option>
                  <option value="1">Ativo</option>
                  <option value="0">Cancelado</option>
                </select>
              </div>
              <div className="form-group">
                <label htmlFor="inputStatus">Categoria</label>
                <select value={product.category} onChange={e => { onProductChange('category', e.target.value); }} className="form-control custom-select">
                  <option selected disabled>Selecione um</option>
                  <option value="Todas">Categorias</option>
                  <option value="Animais de Estimação e animais">Animais de Estimação e animais</option>
                  <option value="Artes e Entretenimento">Artes e Entretenimento</option>
                  <option value="Automóveis e veículos">Automóveis e veículos</option>
                  <option value="Casa e jardim">Casa e jardim</option>
                  <option value="Ciência">Ciência</option>
                  <option value="Comercial e industrial">Comercial e industrial</option>
                  <option value="Comida e bebida">Comida e bebida</option>
                  <option value="Compras">Compras</option>
                  <option value="Computadores e aparelhos eletrônicos">Computadores e aparelhos eletrônicos</option>
                  <option value="Comunidades online">Comunidades online</option>
                  <option value="Condicionamento físico e beleza">Condicionamento físico e beleza</option>
                  <option value="Empregos e Educação">Empregos e Educação</option>
                  <option value="Esportes ">Esportes</option>
                  <option value="Finanças">Finanças</option>
                  <option value="Hobbies e lazer">Hobbies e lazer</option>
                  <option value="Imóveis">Imóveis</option>
                  <option value="Internet e Telecomunicações">Internet e Telecomunicações</option>
                  <option value="Jogos">Jogos</option>
                  <option value="Lei e governo">Lei e governo</option>
                  <option value="Livros e Literatura">Livros e literatura</option>
                  <option value="Noticias">Noticias</option>
                  <option value="Pessoas e sociedade">Pessoas e Sociedade</option>
                  <option value="Referência">Referência</option>
                  <option value="Saúde">Saúde</option>
                  <option value="Viajens">Viajens</option>
                  <option></option>
                </select>
              </div>
              <div className="form-group">
                <label htmlFor="inputSpentBudget">Total em estoque</label>
                <input value={product.stock} onChange={e => { onProductChange('stock', parseInt(e.target.value)); }} type="number" id="inputSpentBudget" className="form-control" step="1" />
              </div>
              <input type="submit" value="Salvar" onClick={(e) => handleSubmit(e)} className="btn btn-success" />
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default ProductForm;
