import React, { useState, useEffect } from 'react';

import api from '../../services/api';

function ProductList() {
  const [list, setList] = useState([]);

  useEffect(() => {
    async function getList() {
      const request = await api.get('products');

      setList(request.data);
    }
    getList();
  }, []);

  return (
    <section className="content">
      <div className="card">
        <div className="card-header">
          <h3 className="card-title">Projects</h3>

          <div className="card-tools">
            <button type="button" className="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i className="fas fa-minus"></i></button>
            <button type="button" className="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i className="fas fa-times"></i></button>
          </div>
        </div>
        <div className="card-body p-0">
          <table className="table table-striped projects">
            <thead>
              <tr>
                <th style={{ width: "1%" }} className="text-center">
                  #
                  </th>
                <th style={{ width: "20%" }} className="text-center">
                  Nome do Produto
                  </th>
                <th  style={{ width: "20%" }} className="text-center">
                  Estoque
                  </th>
                <th style={{ width: "15%" }} className="text-center">
                  Categoria
                  </th>
              </tr>
            </thead>
            <tbody>
              {list.map((d: any) => (
                <tr>
                  <td className="text-center">#</td>
                  <td className="text-center">
                    <a>{d.name}</a>
                  </td>
                  <td className="text-center">
                    {d.stock}
                  </td>
                  <td className="text-center">
                    {d.category}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </section>
  )
}

export default ProductList;
