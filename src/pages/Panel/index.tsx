import React, { useState, useEffect } from 'react';

import api from '../../services/api';

function Panel(props: any) {
  const { search = "" } = props
  const [list, setList] = useState([]);

  useEffect(() => {
    async function getList() {
      const request = await api.get('results', { params: { search } });

      setList(request.data.default.geoMapData);
    }
    getList();
  }, [search]);

  console.log(list);

  return (
    <div className="content-wrapper">
      <div className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1 className="m-0 text-dark">Painel</h1>
            </div>
          </div>

          <div className="card-body p-0">
            <table className="table table-striped projects">
              <thead>
                <tr>
                  <th className="text-center">
                    Nome do Produto
                  </th>
                  <th className="text-center">
                    Código de Localização
                  </th>
                  <th className="text-center">
                    Estado
                  </th>
                  <th className="text-center">
                    Procura
                  </th>
                </tr>
              </thead>
              <tbody>
                {list.map((d: any) => (
                  <tr>
                    <td className="text-center">
                      <a>{search}</a>
                    </td>
                    <td className="text-center">
                      {d.geoCode}
                    </td>
                    <td className="text-center">
                      {d.geoName.substring(8,d.geoName.length)}
                    </td>
                    <td className="text-center">
                      {d.value}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Panel;
