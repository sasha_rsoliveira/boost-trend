import React, { useState } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import Login from './pages/Login';

import Navbar from './components/Navbar';
import Sidebar from './components/Sidebar';

import Panel from './pages/Panel';
import Products from './pages/Products';
import Map from './pages/Map';

function App() {
  const token = localStorage.getItem('Ae!_token');

  const [search, setSearch] = useState('');

  function handLogout() {
    localStorage.removeItem('Ae!_token');
    localStorage.removeItem('Ae!_name');
    window.location.reload();
  }

  if (token) {
    return (
      <BrowserRouter>
        <Navbar search={search} setSearch={setSearch} />
        <Sidebar />
        <Switch>
          <Route path="/panel" component={() => <Panel search={search} />} />
          <Route path="/products" component={Products} />
          <Route path="/map" component={Map} />

          <Redirect from="*" to="/panel" />
        </Switch>
      </BrowserRouter>)
  } else {
    return <Login />
  }

}

export default App;
