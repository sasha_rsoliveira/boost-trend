import axios from 'axios';

let baseURL = 'http://127.0.0.1:3000/api';

if (process.env.REACT_APP_STAGE === 'production') {
  baseURL = 'http://impulsionaae-alonso-eti-br.umbler.net/api';
}

export default axios.create({
  baseURL: baseURL,
  headers: { 'Authorization': localStorage.getItem('Ae!_token') }
})
