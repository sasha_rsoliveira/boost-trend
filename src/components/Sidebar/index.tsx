import React from 'react'
import { NavLink } from 'react-router-dom';

import Logo from '../../assets/logo.png';

function Sidebar() {
  const name = localStorage.getItem('Ae!_name');
  return (
    <aside className="main-sidebar sidebar-dark-primary elevation-4">
      <a href="../paginas/starter.html" className="brand-link">
        <img src={Logo} alt="Logo impulsiona" className="brand-image img-circle elevation-3"
          style={{ opacity: .8 }} />
        <span className="brand-text font-weight-light">Impulsiona Aê!</span>
      </a>
      <div className="sidebar">
        <div className="user-panel mt-3 pb-3 mb-3 d-flex">
          <div className="image">
          </div>
          <div className="info">
            <a href="#" className="d-block">{name}</a>
          </div>
        </div>
        <nav className="mt-2">
          <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <NavLink to="/panel" className="nav-link" activeClassName="active">
              <p>Painel</p>
            </NavLink>
            <NavLink to="/products" className="nav-link" activeClassName="active">
              <p>Produtos</p>
            </NavLink>
            {/* <NavLink to="/map" className="nav-link" activeClassName="active">
              <p>Mapa</p>
            </NavLink> */}
            <li className="nav-item">
              <a href="#" className="nav-link">
                Sair
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </aside>
  )
}

export default Sidebar;
