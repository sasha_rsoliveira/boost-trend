import React, { useState, useEffect } from 'react'
import api from '../../services/api';

function Navbar(props: any) {
  const [list, setList] = useState([]);
  const { search, setSearch } = props;

  useEffect(() => {
    async function getList() {
      const request = await api.get('products');

      setList(request.data);
    }
    getList();
  }, []);
  return (
    <nav className="main-header navbar navbar-expand navbar-white navbar-light">
      <ul className="navbar-nav">
        <li className="nav-item">
          <a className="nav-link" data-widget="pushmenu" href="#" role="button"><i className="fas fa-bars"></i></a>
        </li>
        <li className="nav-item d-none d-sm-inline-block">
          <a href="../paginas/starter.html" className="nav-link">Início</a>
        </li>
      </ul>

      <form className="form-inline ml-3">
        <div className="input-group input-group-sm">
          <select value={search} onChange={(e) => setSearch(e.target.value)} className="form-control form-control-navbar" style={{ width: 150 }}>
            <option value="">Selecione...</option>
            {list.map((d: any) => (
              <option key={d.id} value={d.name}>{d.name}</option>
            ))}
          </select>
        </div>
      </form>
    </nav >
  )
}

export default Navbar;
