interface IfProps {
  test: boolean,
  children: any
}

function If(props: IfProps) {
  if (props.test) {
    return props.children
  } else {
    return false
  }
}

export default If;
